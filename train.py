from collections import defaultdict

import torch
from IPython.display import clear_output
from matplotlib import pyplot as plt
from torch.utils.data import DataLoader

from configs.config import TaskConfig
from src.augmentations import AugsCreation
from src.dataset import SpeechCommandDataset
from src.log_mel_spec import LogMelspec
from src.model import CRNN
from src.sampler import get_sampler, Collator
from src.train import train_epoch, validation


def train(config, train_epoch_fn=train_epoch, teacher_model=None, filename="model.pth"):
    dataset = SpeechCommandDataset(
        path2dir='speech_commands', keywords=TaskConfig.keyword
    )

    indexes = torch.randperm(len(dataset))
    train_indexes = indexes[:int(len(dataset) * 0.8)]
    val_indexes = indexes[int(len(dataset) * 0.8):]

    train_df = dataset.csv.iloc[train_indexes].reset_index(drop=True)
    val_df = dataset.csv.iloc[val_indexes].reset_index(drop=True)

    # Sample is a dict of utt, word and label
    train_set = SpeechCommandDataset(csv=train_df, transform=AugsCreation())
    val_set = SpeechCommandDataset(csv=val_df)

    train_sampler = get_sampler(train_set.csv['label'].values)

    # Here we are obliged to use shuffle=False because of our sampler with randomness inside.

    train_loader = DataLoader(train_set, batch_size=TaskConfig.batch_size,
                              shuffle=False, collate_fn=Collator(),
                              sampler=train_sampler,
                              num_workers=2, pin_memory=True)

    val_loader = DataLoader(val_set, batch_size=TaskConfig.batch_size,
                            shuffle=False, collate_fn=Collator(),
                            num_workers=2, pin_memory=True)

    melspec_train = LogMelspec(is_train=True, config=TaskConfig)
    melspec_val = LogMelspec(is_train=False, config=TaskConfig)

    model = CRNN(config).to(config.device)

    opt = torch.optim.Adam(
        model.parameters(),
        lr=config.learning_rate,
        weight_decay=config.weight_decay
    )

    # TRAIN
    history = defaultdict(list)

    for n in range(config.num_epochs):
        train_epoch_fn(teacher_model, model, opt, train_loader, melspec_train, config)

        au_fa_fr = validation(model, val_loader, melspec_val, config.device)

        if len(history['val_metric']) == 0 or history['val_metric'][-1] > au_fa_fr:
            torch.save(model.state_dict(), filename)

        history['val_metric'].append(au_fa_fr)

        clear_output()
        plt.plot(history['val_metric'])
        plt.ylabel('Metric')
        plt.xlabel('Epoch')
        plt.grid()
        plt.show()

        print('END OF EPOCH', n)

    return model, history


if __name__ == "__main__":
    train()