# KWS

## Installation guide
Install requirements.
```shell
pip install -r ./requirements.txt -q
```
Download dataset
```shell
./download_dataset.sh
```
Download models (`base_model.pth`, `student_model.pth`, `quantized_model.pth`, `quantized_distilled_model.pth`)
```python
from  src.utils import download_models
download_models()
```
Download audio for streaming test (`base_model.pth`, `student_model.pth`, `student_model_attn.pth`, `quantized_model.pth`, `quantized_distilled_model.pth`)
```python
from  src.utils import download_music
download_music()
```

## Base model
Checkpoint is downloaded with all model's checkpoints. Model quality is `2.4e-5`

## Streaming
I tested streaming with window size 128 and step size 32. The audio is a song "Gayazov brother$ - увезите меня на дип-хаус (гачи ремикс)".

![](img/streaming.png)

## Distillation
The experiments can be found in the notebook.

Dark Knowledge loss is defined as in [1].

Distillation with attention reduced both memory and PLOPs a lot, however could not achieve good enough quality in the time I had(`~7.6e-5`).
I have tried different configurations for the model, different temperatures and weights for loss parts.

The first time I checked MACs I got result of x31 improvement on one GPU and x6 on another GPU (different Colabs).
The next time I checked I got x6 again. This shows how unreliable this metric is.

## Quantization

I quantized 2 models of acceptable quality - the base model and distilled model.
Both of them slightly lost in quality, gained in compression.

## Results
Metrics (multiplied by 1e5 for scale) and memory, FLOPs compression (relative to the base model).
x10 memory compression is achieved by all distilled models, x10 speed up is achieved by distilled+attention model.

![](img/results.png)


## References
1. Hinton, Geoffrey, Oriol Vinyals, and Jeff Dean. "Distilling the knowledge in a neural network." arXiv preprint arXiv:1503.02531 (2015).
