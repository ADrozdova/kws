import torch
from torch import nn
import torch.nn.functional as F
from tqdm import tqdm

from src.metrics import count_FA_FR, get_au_fa_fr


def loss_fn_kd(outputs, labels, teacher_outputs, alpha, temperature):
    KD_loss = nn.KLDivLoss(reduction='batchmean')(F.log_softmax(outputs/temperature, dim=1),
                             F.softmax(teacher_outputs/temperature, dim=1)) * (alpha * temperature ** 2) + \
              F.cross_entropy(outputs, labels) * (1. - alpha)

    return KD_loss


def distill_train_epoch(teacher, student, opt, loader, log_melspec, config):
    alpha = config.alpha
    temperature = config.temperature
    device = config.device

    student.train()
    teacher.eval()
    val_losses, accs, FAs, FRs = [], [], [], []
    all_probs, all_labels = [], []

    for i, (batch, labels) in tqdm(enumerate(loader), total=len(loader)):
        batch, labels = batch.to(device), labels.to(device)
        batch = log_melspec(batch)

        opt.zero_grad()

        with torch.no_grad():
            teacher_logits = teacher(batch)
        student_logits = student(batch)

        loss = loss_fn_kd(student_logits, labels, teacher_logits, alpha, temperature)

        if config.distill_attn:
            teacher_attn = teacher.get_attn_alpha()
            student_attn = student.get_attn_alpha()
            loss += config.beta * nn.KLDivLoss(reduction='batchmean')(torch.log(student_attn), teacher_attn)

        loss.backward()
        torch.nn.utils.clip_grad_norm_(student.parameters(), 5)
        opt.step()

        # logging
        probs = F.softmax(student_logits, dim=-1)
        argmax_probs = torch.argmax(probs, dim=-1)
        all_probs.append(probs[:, 1].cpu())
        all_labels.append(labels.cpu())
        val_losses.append(loss.item())
        accs.append(
            torch.sum(argmax_probs == labels).item() /  # ???
            torch.numel(argmax_probs)
        )
        FA, FR = count_FA_FR(argmax_probs, labels)
        FAs.append(FA)
        FRs.append(FR)

    au_fa_fr = get_au_fa_fr(torch.cat(all_probs, dim=0).cpu(), all_labels)
    return au_fa_fr
