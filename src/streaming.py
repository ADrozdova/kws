import torch
import torch.nn.functional as F


class StreamKWS:
    def __init__(self, model, max_window_length, step):
        self.model = model
        self.max_window_length = max_window_length
        self.step = step

    def next_step(self, input, hidden):
        output = self.model(input, hidden)
        output = F.softmax(output, dim=-1)
        return output

    def stream(self, input):
        self.model.eval()
        probs = []

        self.model.hidden_ = None

        with torch.no_grad():
            # process frame by frame
            for frame in range(0, input.shape[2] - self.max_window_length, self.step):
                p = self.next_step(input[:, :, frame:frame + self.max_window_length], self.model.get_hidden())
                probs.append(p)
        return torch.cat(probs, dim=0)
