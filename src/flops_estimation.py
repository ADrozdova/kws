import torch
from thop import profile  # !pip install thop
from torch import nn


class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Conv1d(1, 1, 3, bias=False)

    def forward(self, x):
        return self.model(x)

def flopsEstimantion(model):
    profile(model, (torch.randn(1, 1, 4),)) # -> (6.0 MACs, 3.0 parameters) for Model()
