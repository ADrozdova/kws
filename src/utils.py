from google_drive_downloader import GoogleDriveDownloader as gdd
import os

# https://drive.google.com/file/d/1Ul92R0XAYjFH_F-NASzB9oBPztI9WzRR/view?usp=sharing - base
# https://drive.google.com/file/d/1IWO0--HUwt91PXxOwNR8XgtFgOLzyPJ4/view?usp=sharing - quantized distilled
# https://drive.google.com/file/d/1R2PhOHbNW1kBXEZs8ICiQfV8c--mD2wb/view?usp=sharing - quantized
# https://drive.google.com/file/d/1aosxgaCaWLjXW-gcDJgtTYr67-yXM6FT/view?usp=sharing - distilled
# https://drive.google.com/file/d/1Xmz1Zb6_1b7oj920YsDtQHQW_z1KIuB4/view?usp=sharing

def download_models():
    gdd.download_file_from_google_drive(file_id="1Ul92R0XAYjFH_F-NASzB9oBPztI9WzRR",  # base
                                        dest_path="./base_model.pth")
    gdd.download_file_from_google_drive(file_id="1IWO0--HUwt91PXxOwNR8XgtFgOLzyPJ4",  # quantized distilled
                                        dest_path="./quantized_distilled_model.pth")
    gdd.download_file_from_google_drive(file_id="1R2PhOHbNW1kBXEZs8ICiQfV8c--mD2wb",  # quantized
                                        dest_path="./quantized_model.pth")
    gdd.download_file_from_google_drive(file_id="1aosxgaCaWLjXW-gcDJgtTYr67-yXM6FT",  # distilled
                                        dest_path="./student_model.pth")
    gdd.download_file_from_google_drive(file_id="1Xmz1Zb6_1b7oj920YsDtQHQW_z1KIuB4",  # distilled attn
                                        dest_path="./student_model_attn.pth")

# https://drive.google.com/file/d/1tnh5-IMdDXKc0Bd1Y8o2vN4a6rVeq-Km/view?usp=sharing


def download_music():
    gdd.download_file_from_google_drive(file_id="1tnh5-IMdDXKc0Bd1Y8o2vN4a6rVeq-Km",
                                        dest_path="./gayazov_brothers_deep_house.wav")
